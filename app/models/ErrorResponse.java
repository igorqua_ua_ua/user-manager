package models;

public class ErrorResponse {
    public final String error;

    public ErrorResponse(String error) {
        this.error = error;
    }
}
