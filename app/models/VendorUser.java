package models;

import org.mongodb.morphia.annotations.Entity;

@Entity(noClassnameStored = true)
public class VendorUser {

    private String id;
    private String login;
    private boolean activation;

    public VendorUser(String id, String login, boolean activation) {
        this.id = id;
        this.login = login;
        this.activation = activation;
    }

    public String getId() {
        return id;
    }

    public VendorUser(){}

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isActivation() {
        return activation;
    }

    public void setActivation(boolean activation) {
        this.activation = activation;
    }
}
