package dao;

import dao.finders.Finder;
import mongo.MongoDB;
import org.mongodb.morphia.query.Query;

import java.lang.reflect.ParameterizedType;

public abstract class AbstractDao<T> {

    private Class<T> tClass;

    public AbstractDao() {
        this.tClass = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    public long getCount() {
        return MongoDB.datastore.getCount(tClass);
    }

    public void save(T val) {
        MongoDB.datastore.save(val);
    }

    public void deleteAll() {
        MongoDB.datastore.delete(MongoDB.datastore.createQuery(tClass));
    }

    @SafeVarargs
    public final void deleteItems(T... data) {
        for (T t : data) {
            MongoDB.datastore.delete(t);
        }
    }
    public Query<T> getQuery() {
        return MongoDB.datastore.find(tClass);
    }

    public abstract Finder<T> finder();
}
