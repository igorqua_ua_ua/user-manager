package dao;

import com.google.inject.Singleton;
import dao.finders.UserFinder;
import models.User;

/**
 * Created by alex on 16.07.15.
 */
@Singleton
public class UserDao extends AbstractDao<User> {
    @Override
    public UserFinder finder() {
        return new UserFinder(this);
    }
}
