package dao.finders;

import dao.AbstractDao;
import org.mongodb.morphia.query.Query;

import java.util.List;

/**
 * Created by alex on 15.07.15.
 */
public class Finder<T> {
    private Query<T> query;

    public Finder(AbstractDao<T> dao) {
        this.query = dao.getQuery();
    }

    protected Query<T> getQuery() {
        return query;
    }

    public List<T> getList() {
        return query.asList();
    }

    public long get(List<T> result) {
        result.addAll(query.asList());
        return query.countAll();
    }

    public T get(){
        return query.get();
    }

}
