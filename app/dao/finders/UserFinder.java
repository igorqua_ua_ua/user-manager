package dao.finders;

import dao.AbstractDao;
import models.User;

public class UserFinder extends Finder<User> {
    public UserFinder(AbstractDao<User> dao) {
        super(dao);
    }

    public UserFinder user(String name) {
        getQuery().field("name").equal(name);
        return this;
    }
}
