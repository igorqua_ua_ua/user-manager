package utils;

import play.Logger;
import play.api.mvc.RequestHeader;
import play.mvc.Http;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by alex on 29.09.15.
 */
public class LoggerHelper {
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private final StringBuilder logStringBuilder = new StringBuilder();

    public static LoggerHelper log(Http.RequestHeader request) {
        return logScala(request._underlyingHeader());
    }

    public static <T> LoggerHelper logScala(RequestHeader request) {
        return new LoggerHelper(request);
    }

    private <T> LoggerHelper(RequestHeader request) {
        logStringBuilder.append(formatter.format(new Date()))
                .append(" ").append(request.path().replace("/", " "));
        Map<String, Seq<String>> params = JavaConverters.mapAsJavaMapConverter(request.queryString()).asJava();
        for (Map.Entry<String, Seq<String>> param : params.entrySet()) {
            if (!"_".equals(param.getKey())) {
                logStringBuilder
                        .append(" ").append(param.getKey())
                        .append("=").append(param.getValue().head());
            }
        }
    }

    public LoggerHelper append(String str) {
        logStringBuilder.append(" ").append(str);
        return this;
    }

    public void flush() {
        Logger.of("client-admin").info(logStringBuilder.toString());
    }
}
