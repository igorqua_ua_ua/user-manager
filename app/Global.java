import dao.UserDao;
import models.User;
import mongo.MongoDB;
import play.Application;
import play.GlobalSettings;
import play.Play;

public class Global extends GlobalSettings {
    @Override
    public void onStart(Application app) {
        super.onStart(app);
        MongoDB.connect();
        addAdminUser();
    }

    private void addAdminUser() {
        MongoDB.datastore.getCollection(User.class).drop();
        UserDao userDao = new UserDao();
        userDao.save(new User(readAdminNameConfig(), readAdminPwConfig()));
    }

    private static String readAdminNameConfig() {
        return Play.application().configuration().getString("admin.name");
    }

    private static String readAdminPwConfig() {
        return Play.application().configuration().getString("admin.pw");
    }

    @Override
    public void onStop(Application application) {
        super.onStop(application);
        MongoDB.disconnect();
    }
}