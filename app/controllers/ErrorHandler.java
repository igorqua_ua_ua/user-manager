package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.ErrorResponse;
import play.Configuration;
import play.Environment;
import play.Logger;
import play.api.OptionalSourceMapper;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.libs.F;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import utils.LoggerHelper;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.function.Function;

public class ErrorHandler extends DefaultHttpErrorHandler {

    @Inject
    public ErrorHandler(Configuration configuration, Environment environment,
                        OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(configuration, environment, sourceMapper, routes);
    }

    @Override
    protected F.Promise<Result> onForbidden(Http.RequestHeader requestHeader, String s) {
        return makeResult(requestHeader, "Forbidden", Results::forbidden, (r) -> super.onForbidden(requestHeader, s));
    }

    @Override
    protected F.Promise<Result> onNotFound(Http.RequestHeader requestHeader, String s) {
        return makeResult(requestHeader, "Not Found", Results::notFound, (r) -> super.onNotFound(requestHeader, s));
    }

    @Override
    protected F.Promise<Result> onOtherClientError(Http.RequestHeader requestHeader, int i, String s) {
        return onBadRequest(requestHeader, "Other Client Error");
    }

    @Override
    protected F.Promise<Result> onBadRequest(Http.RequestHeader requestHeader, String s) {
        return makeResult(requestHeader, s, Results::badRequest, (r) -> super.onNotFound(requestHeader, s));
    }

    @Override
    public F.Promise<Result> onServerError(Http.RequestHeader request, Throwable exception) {
        if (isClient(request)) {
            if (exception instanceof BadRequestException) {
//                Logger.error(request.uri(), exception.getMessage());
                LoggerHelper.log(request).append("client error: " + exception.getMessage()).flush();
                return makeResult(Results::badRequest, exception.getMessage());
            } else {
                Logger.error(request.uri(), exception);
                return makeResult(Results::internalServerError, "Internal server error");
            }
        } else {
            return super.onServerError(request, exception);
        }
    }


    private F.Promise<Result> makeResult(Http.RequestHeader requestHeader, String s, Function<JsonNode, Result> clientFunc, Function<Void, F.Promise<Result>> defaultFunc) {
        if (isClient(requestHeader)) {
            LoggerHelper.log(requestHeader).append("client error: " + s).flush();
            return makeResult(clientFunc, s);
        } else {
            return defaultFunc.apply(null);
        }
    }

    private F.Promise<Result> makeResult(Function<JsonNode, Result> f, String s) {
        return  F.Promise.pure(f.apply(Json.toJson(new ErrorResponse(s))));
    }

    private boolean isClient(Http.RequestHeader requestHeader) {
        return requestHeader.path().startsWith("/client");
    }
}