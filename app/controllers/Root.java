package controllers;


import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.root;
import views.html.users;

@Security.Authenticated(SecurePage.class)
public class Root extends Controller{
    public Result users() {
        return ok(root.render(users.render()));
    }
}
