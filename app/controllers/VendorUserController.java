package controllers;

import dao.VendorUserDao;
import models.VendorUser;
import play.mvc.Controller;
import play.mvc.Result;

public class VendorUserController extends Controller {
    public Result addUser() {
        play.data.Form<VendorUser> form = play.data.Form.form(VendorUser.class).bindFromRequest();
        VendorUser vendorUser = form.get();
        VendorUserDao vendorUserDao = new VendorUserDao();
        vendorUserDao.save(vendorUser);
        return redirect("/admin/users");
    }
}
