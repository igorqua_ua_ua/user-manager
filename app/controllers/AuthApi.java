package controllers;

import com.google.inject.Inject;
import dao.UserDao;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login$;

public class AuthApi extends Controller {
    @Inject
    private UserDao dao;

    public Result authForm() {
        return ok(login$.MODULE$.render(Form.form(User.class)));
    }

    public Result login() {
        Form<User> userForm = Form.form(User.class).bindFromRequest();
        if (!userForm.hasErrors()) {
            User user = userForm.get();
            User storedUser = dao.finder().user(user.getName()).get();
            if (storedUser != null && storedUser.getPassword().equals(user.getPassword())) {
                ctx().session().put("username", user.getName());
                return redirect("/admin/users");
            } else {
                userForm.reject("Bad username or password");
            }
        }
        return badRequest(login$.MODULE$.render(userForm));
    }

    public Result logout() {
        ctx().session().clear();
        return redirect("/login");
    }

}
