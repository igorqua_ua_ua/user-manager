package controllers;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SecurePage extends Security.Authenticator {
    @Override
    public Result onUnauthorized(Http.Context ctx) {
        try {
            return redirect("/login?url=" + URLEncoder.encode(ctx.request().uri(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
